# README #

This is the Project Management Task and Issues tracker. It doesn't include any source files anymore.

### About Pipelines ###

Continuous integration allows the developer to make use of the remote machine to implement long tasks such as testing and deployment. It may continously track on the git repositories.

The actual functionality used here is a deployment to Sonatype's Nexus Central Repository https://oss.sonatype.org as Snapshots (ossrh).

Here follow the files needed by the project to deploy.

Additionally, the developer defines 3 environment variables :

  a. *CIPHER\_PWD=enCrypTed* : encrypted certificates
  b. *CI\_DEPLOY\_USERNAME* and *CI\_DEPLOY\_PASSWORD* : Nexus credentials accordingly to an existing Sonatype's account. 

If __Tags__ with __release-*__ deploy with GPG signatures, 3 more variables need to be defined :

  c. *OPENSSL\_PWD* : your private-key.asc got encrypted with openSSL
  d. *GPG\_KEY* *GPG\_PWD* : GPG key name and password.

* More about these settings in [Kernel Readme file](https://bitbucket.org/b23prodtm/sf3-jxakn/overview).
* More about Maven system properties and environment variables [here](http://books.sonatype.com/mvnref-book/reference/resource-filtering-sect-properties.html) and below CI settings files.
* The configuration file deploys with the settings.xml file that must be found on a separate branch, named 'travis'.

## bitbucket-pipelines.yml ##

```
#!yaml
# This is a sample build configuration for Java (Maven).
# Check our guides at https://confluence.atlassian.com/x/zd-5Mw for more examples.
# Only use spaces to indent your .yml configuration.
# -----
# You can specify a custom docker image from Docker Hub as your build environment.
image: maven:3.3.9-jdk-8
pipelines:
  default:
    - step:
        caches:
          - maven
        script: # Modify the commands below to build your repository.
          - mvn -B verify # -B batch mode makes Maven less verbose          
  tags:                         # To invoke the Nexus Staging Release plugin
    name-as-maven-artifactid-*: # call a mvn release:clean release:prepare.
      - step:                   # specify the git tag release-X.X.X after that
          caches:
            - maven
          name: Perform Release through OpenSSL to Nexus
          script: 
            # go to deploy release-* tags : release process with encrypted private key.
            - openssl aes-256-cbc -pass pass:$OPENSSL_PWD -in .circle/private-key.gpg.enc -out .circle/private-key.gpg -d
            - gpg --import .circle/private-key.gpg
            # the project is supposed to have been already prepared for performing release 
            - mvn -V -B -s .circle/settings.xml release:perform
  branches:
    master:
      - step:
          caches:
            - maven
          name: Deploy Snapshots to Nexus
          script: 
            - mvn -V -B -s .circle/settings.xml deploy

```

It allows to continously push **-SNAPSHOTS** versions and __release-*__ tags to Nexus Central. 
_
See more about how to define **private** project environment variables with Pipelines [here](https://bitbucket.org/simpligility/ossrh-pipeline-demo)

### FAQ
        
I've got a library dependency not found in maven central repository, e.g. my-ant-lib.jar how can I add it to the project ?

Open a shell Terminal and browse to project directory.
Do one build attempt with _mvn install_ .
A folder named target/ant-build-files should have been created that way.
add execute permission :

  * chmod target/ant-build-files/mv2importlib.sh

run the shell script (input with absolute paths where needed)

  * sh mvn2importlib.sh <destAbsolutePath> <sourceFolder>  <sourceFilename> <groupId> <artifactId> <version> 

*maven-deploy-plugin* should return successfully and the required lib/folder structure will be created for the project.
The following repository addition must be added to pom.xml
        
```
<!--other repositories if any-->
                <repository>
                        <id>project.local</id>
                        <name>libRepo</name>
                        <url>file:${basedir}/lib</url>
                </repository>
```
```
<!-- add other libraries e.g. for any library found in lib/org/ant/my-ant-lib/1.0 -->
                <dependency>
                        <groupId>org.ant</groupId>
                        <artifactId>my-ant-lib</artifactId>
                        <version>1.0</version>
                </dependency>
```
This is about configuring a remote settings file on git to deploy artifacts on Central Repository with the continuous integration system.

www.b23prodtm.info